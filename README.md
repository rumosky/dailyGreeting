# 说明

国内地址：https://gitee.com/rumosky/dailyGreeting

GitHub地址：https://github.com/rumosky/dailyGreeting

为女友写的小脚本，挂在服务器上，具体使用方法，请访问：https://rumosky.com/archives/112

**2024年10月4日更新**

增加了数据持久化方案，可以记录已发送的情话，每天随机，不重样

地址：https://rumosky.com/archives/833